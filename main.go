package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// var mongoURI = "mongodb+srv://itbiz:BizBoyz@itbiz.xxcrb.mongodb.net/bestptofile-cvtheque?retryWrites=true&w=majority"

var mongoURI = "mongodb://root:Azer1234@cvtheque-db:27017/?retryWrites=true&w=majority"

// var dbName = "bestptofile-cvtheque"

var dbName = "cvtheque-db"

func mongoCon() *mongo.Client {

	clientOptions := options.Client().ApplyURI(mongoURI)
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		fmt.Println(err)
	}

	err = client.Ping(context.TODO(), nil)

	if err != nil {
		fmt.Println(err)
	}

	log.Println("Connected to MongoDB!")

	// // Don't forget to logout
	// defer client.Disconnect(context.TODO())
	return client
}

var MongoClient = mongoCon()

func submitreq(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; text/html; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST")
	w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, X-Requested-With, X-CSRF-Token")

	file, handler, err := r.FormFile("file")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	fmt.Println(file)
	whenRec := time.Now()
	handler.Filename = strings.ReplaceAll(r.FormValue("fullname"), " ", "_") + "-" + strings.ReplaceAll(handler.Filename, " ", "_")
	f, err := os.OpenFile("static/"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	// _, _ = io.WriteString(w, "File "+handler.Filename+" Uploaded successfully")
	_, _ = io.Copy(f, file)

	filter := bson.M{
		"from":    r.FormValue("fullname") + " <" + r.FormValue("email") + ">",
		"city":    r.FormValue("city"),
		"profile": strings.ToLower(r.FormValue("profile")),
	}

	submitted_profile := bson.D{{"$set",
		bson.D{
			{"date", whenRec},
			{"from", r.FormValue("fullname") + " <" + r.FormValue("email") + ">"},
			{"city", r.FormValue("city")},
			{"profile", strings.ToLower(r.FormValue("profile"))},
			{"experience", r.FormValue("experience")},
			{"languages_array", strings.Split(r.FormValue("languages"), ",")},
			{"languages", r.FormValue("languages")},
			{"filename", handler.Filename},
			{"isTreated", true},
			{"isCrawled", true},
		},
	}}

	collection := MongoClient.Database(dbName).Collection("profile")
	opts := options.Update().SetUpsert(true)

	// _, err = collection.InsertOne(context.TODO(), submitted_profile)
	_, err = collection.UpdateOne(context.TODO(), filter, submitted_profile, opts)

	if err != nil {
		log.Fatal(err)
	}

	// json.NewEncoder(w).Encode(r.FormValue("fullname") + " <" + r.FormValue("email") + "> " + "Submitted " + handler.Filename + " Successfully")
	json.NewEncoder(w).Encode("Data Submitted Successfully")
	w.WriteHeader(http.StatusOK)
}

// Main function
func main() {
	// Init router
	r := mux.NewRouter()

	// header := handlers.AllowedHeaders([]string{"Accept", "X-Requested-With", "Content-Type", "Authorization", "X-CSRF-Token"})
	// methods := handlers.AllowedMethods([]string{"GET", "POST"})
	// origins := handlers.AllowedOrigins([]string{"http://localhost:4200/*"})

	r.HandleFunc("/submitreq", submitreq).Methods("POST", "OPTIONS")

	// Start server
	// log.Fatal(http.ListenAndServe(":3000", handlers.CORS(header, methods, origins)(r)))
	log.Fatal(http.ListenAndServe(":5000", r))
}
